package com.bakaeva.tm.controller;

import com.bakaeva.tm.api.ICommandService;
import com.bakaeva.tm.model.TerminalCommand;
import com.bakaeva.tm.util.NumberUtil;

import java.util.Arrays;

public class ICommandController implements com.bakaeva.tm.api.ICommandController {
    private final ICommandService commandService;

    public ICommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alina Bakaeva");
        System.out.println("E-mail: Bak_Al@bk.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.10");
    }

    @Override
    public void showArguments() {
        final String[] arguments = commandService.getArguments();
        System.out.println(Arrays.toString(arguments));
    }

    @Override
    public void showCommands() {
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = commandService.getTerminalCommands();
        for (final TerminalCommand command : commands) System.out.println(command);
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
