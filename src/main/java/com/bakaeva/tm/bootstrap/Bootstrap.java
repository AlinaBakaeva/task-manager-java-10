package com.bakaeva.tm.bootstrap;

import com.bakaeva.tm.api.ICommandRepository;
import com.bakaeva.tm.api.ICommandService;
import com.bakaeva.tm.constant.ArgumentConst;
import com.bakaeva.tm.constant.TerminalConst;
import com.bakaeva.tm.controller.ICommandController;
import com.bakaeva.tm.repository.CommandRepository;
import com.bakaeva.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new ICommandController(commandService);

    public void run(final String[] args) {
        System.out.println("*** Welcome to task manager ***");
        if (parseArguments(args)) commandController.exit();
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseCommand(command);
        }

    }

    public void parseArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    public void parseCommand(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    public boolean parseArguments(final String[] arguments) {
        if (arguments == null || arguments.length == 0) return false;
        final String argument = arguments[0];
        parseArgument(argument);
        return true;
    }

}
