package com.bakaeva.tm.service;

import com.bakaeva.tm.api.ICommandRepository;
import com.bakaeva.tm.api.ICommandService;
import com.bakaeva.tm.model.TerminalCommand;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public TerminalCommand[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public String[] getArguments() {
        return commandRepository.getArgs();
    }
}
