package com.bakaeva.tm.api;

import com.bakaeva.tm.model.TerminalCommand;

public interface ICommandService {

    public TerminalCommand[] getTerminalCommands();

    public String[] getCommands();

    public String[] getArguments();

}
