package com.bakaeva.tm.api;

import com.bakaeva.tm.model.TerminalCommand;

public interface ICommandRepository {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
